package com.sport.sportmg;

import com.sport.sportmg.controller.MatchCtrl;
import com.sport.sportmg.model.Match;
import com.sport.sportmg.utils.Utils;

import org.junit.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class UnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void comparePassword() throws NoSuchAlgorithmException {
        String hashed = "a08d56bb4753f33bc1cabc063f6e4a298173a5cd";
        assertTrue(Utils.matchPassword("bary", hashed));
        assertEquals(hashed, Utils.sha1("bary"));
    }

    @Test
    public void getMatch() throws Exception {
        Match[] matchs = new MatchCtrl().getMatch(5, null);
        assertNotNull(matchs);
    }
}