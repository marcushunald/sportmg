package com.sport.sportmg.controller;

import com.sport.sportmg.model.User;
import com.sport.sportmg.utils.Utils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserCtrl {
	public static final String USER_PREFERENCE = "userPreference" ;
	public static final String USER_NAME = "username";
	public static final String USER_ID = "userId";
	public static final String USER_EMAIL = "email";

	public User login(String login, String pwd) throws Exception {
		User user = null;
		Dao dao = new Dao();
		List<User> users = dao.select("users", "login", login, User.class);
		if(users == null || users.size() == 0) return null;
		user = users.get(0);
		if(Utils.matchPassword(pwd, user.getPwd()))
			return user;
		return null;
	}

	public User getUser(String id) throws Exception {
		return new Dao().select("users", id, User.class);
	}

	public boolean signUp(String login, String pwd, String email) throws Exception {
		User user = new User(login, Utils.sha1(pwd), email);
		Dao dao = new Dao();
		return dao.insert("users", user);
	}

	public static boolean checkPwdConf(String pwd, String pwdConf) {
		return (pwd.equals(pwdConf));
	}

	public boolean updatePwd(String id, String newPwd) throws Exception {
		Dao dao = new Dao();
		return dao.update("users", id, "pwd", newPwd);
	}

	public static boolean checkEmail(String email) {
		String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
}
