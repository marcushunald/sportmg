package com.sport.sportmg.controller;

import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Objects;

public class Test {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    public void getUsers() {
        db.collection("users")
                .get()
                .addOnCompleteListener(this::onComplete);
    }

    private void onComplete(Task<QuerySnapshot> task) {
        if(task.isSuccessful()) {
            Log.d("Doc", "================================");
            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                Log.d("Doc", String.format("%s => %s\n", document.getId(), document.getData()));
            }
        } else {
            Log.d("Err", "================================");
            Log.w("Err", "Error getting documents", task.getException());
        }
    }
}
