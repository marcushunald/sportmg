package com.sport.sportmg.controller;

import com.sport.sportmg.model.Equipe;

import java.util.List;

public class EquipeCtrl {
	public Equipe[] getEquipes() throws Exception {
		Dao dao = new Dao();
		List<Equipe> equipes = dao.select("equipes", Equipe.class);
		return equipes.toArray(new Equipe[0]);
	}
}
