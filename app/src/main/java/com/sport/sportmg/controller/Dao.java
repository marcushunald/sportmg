package com.sport.sportmg.controller;

import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.common.collect.Lists;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeoutException;

public class Dao {
	private final long TIMEOUT = 1000000000;
	private final FirebaseFirestore db = FirebaseFirestore.getInstance();

	public <T> List<T> select(String collection, Class<T> type) throws Exception {
		Task<QuerySnapshot> task = db.collection(collection).get();
		double waiting = 0;
		while(!task.isComplete() && waiting <= TIMEOUT)
			waiting++;
		if(waiting > TIMEOUT)
			throw new TimeoutException(String.format("Out of time for getting %s", collection));
		List<T> result = new ArrayList<>();
		for(DocumentSnapshot documentSnapshot : Objects.requireNonNull(task.getResult())) {
			T element = documentSnapshot.toObject(type);
			try {
				Method setId = type.getMethod("setId", String.class);
				if(setId != null) {
					setId.invoke(element, documentSnapshot.getId());
				}
			} catch(Exception e) {
			}
			result.add(element);
		}
		return result;
	}

	public <T> T select(String collection, String id, Class<T> type) throws Exception {
		Task<DocumentSnapshot> task = db.collection(collection).document(id).get();
		double waiting = 0;
		while(!task.isComplete() && waiting <= TIMEOUT)
			waiting++;
		if(waiting > TIMEOUT)
			throw new TimeoutException(String.format("Out of time for getting %s", collection));
		DocumentSnapshot documentSnapshot = task.getResult();
		T element = documentSnapshot.toObject(type);
		try {
			Method setId = type.getMethod("setId", String.class);
			if(setId != null) {
				setId.invoke(element, documentSnapshot.getId());
			}
		} catch(Exception e) {
		}
		return element;
	}

	public <T> List<T> select(String collection, String column, Object value, Class<T> type) throws Exception {
		Task<QuerySnapshot> task = db.collection(collection).whereEqualTo(column, value).get();
		double waiting = 0;
		while(!task.isComplete() && waiting <= TIMEOUT)
			waiting++;
		if(waiting > TIMEOUT)
			throw new TimeoutException(String.format("Out of time for getting %s", collection));
		List<T> result = new ArrayList<>();
		for(DocumentSnapshot documentSnapshot : Objects.requireNonNull(task.getResult())) {
			T element = documentSnapshot.toObject(type);
			try {
				Method setId = type.getMethod("setId", String.class);
				if(setId != null) {
					setId.invoke(element, documentSnapshot.getId());
				}
			} catch(Exception e) {
			}
			result.add(element);
		}
		return result;
	}

	public <T> List<T> selectIgnoreCase(String collection, String column, Object value, Class<T> type) throws Exception {
//		Task<QuerySnapshot> task = db.collection(collection).orderBy(column).startAt(value).endAt(value + "\uf8ff").get();
		Task<QuerySnapshot> task = db.collection(collection).whereEqualTo(column, value).get();
		double waiting = 0;
		while(!task.isComplete() && waiting <= TIMEOUT)
			waiting++;
		if(waiting > TIMEOUT)
			throw new TimeoutException(String.format("Out of time for getting %s", collection));
		List<T> result = new ArrayList<>();
		for(DocumentSnapshot documentSnapshot : Objects.requireNonNull(task.getResult())) {
			T element = documentSnapshot.toObject(type);
			try {
				Method setId = type.getMethod("setId", String.class);
				if(setId != null) {
					setId.invoke(element, documentSnapshot.getId());
				}
			} catch(Exception e) {
			}
			result.add(element);
		}
		return result;
	}

	public <T> List<T> select(String collection, String column, Object start, Object end, int limit, Class<T> type) throws Exception {
		Query query = db.collection(collection);
		if(start != null)
			query = query.whereGreaterThanOrEqualTo(column, start);
		if(end != null)
			query = query.whereLessThanOrEqualTo(column, end);
		if(limit > 0)
			query = query.limit(limit);
		Task<QuerySnapshot> task = query.get();
		double waiting = 0;
		while(!task.isComplete() && waiting <= TIMEOUT)
			waiting++;
		if(waiting > TIMEOUT)
			throw new TimeoutException(String.format("Out of time for getting %s", collection));
		List<T> result = new ArrayList<>();
		for(DocumentSnapshot documentSnapshot : Objects.requireNonNull(task.getResult())) {
			T element = documentSnapshot.toObject(type);
			try {
				Method setId = type.getMethod("setId", String.class);
				if(setId != null) {
					setId.invoke(element, documentSnapshot.getId());
				}
			} catch(Exception e) {
			}
			result.add(element);
		}
		return result;
	}

	public boolean insert(String collection, Object object) throws Exception {
		Task<DocumentReference> task = db.collection(collection).add(object);
		double waiting = 0;
		while(!task.isComplete() && waiting <= TIMEOUT)
			waiting++;
		if(waiting > TIMEOUT)
			throw new Exception(String.format("Out of time for setting %s", collection));
		return task.isSuccessful();
	}

	public boolean update(String collection, String id, String column, String pwd) throws Exception {
		Task<Void> task = db.collection(collection).document(id).update(column, pwd);
		double waiting = 0;
		while(!task.isComplete() && waiting <= TIMEOUT)
			waiting++;
		if(waiting > TIMEOUT)
			throw new Exception(String.format("Out of time for setting %s", collection));
		return task.isSuccessful();
	}

	public <T> List<T> select(String collection, String domicile, String exterieur, List<String> ids, Class<T> type) throws TimeoutException {
//		List<List<String>> lists = Lists.partition(ids, 10);
//		List<T> result = new ArrayList<>();
//		for(int i = 0; i < lists.size(); i++) {
//			Task<QuerySnapshot> task = db.collection(collection).whereIn(domicile, lists.get(i)).whereIn(exterieur, lists.get(i)).get();
//			double waiting = 0;
//			while(!task.isComplete() && waiting <= TIMEOUT)
//				waiting++;
//			if(waiting > TIMEOUT)
//				throw new TimeoutException(String.format("Out of time for getting %s", collection));
//			for(DocumentSnapshot documentSnapshot : Objects.requireNonNull(task.getResult())) {
//				T element = documentSnapshot.toObject(type);
//				try {
//					Method setId = type.getMethod("setId", String.class);
//					if(setId != null) {
//						setId.invoke(element, documentSnapshot.getId());
//					}
//				} catch(Exception e) {
//
//				}
//				result.add(element);
//			}
//		}
		List<T> result = new ArrayList<>();
		result.addAll(this.select(collection, domicile, ids,type));
		result.addAll(this.select(collection, exterieur, ids,type));
		return result;
	}

	public <T> List<T> select(String collection, String position, List<String> ids, Class<T> type) throws TimeoutException {
		List<List<String>> lists = Lists.partition(ids, 10);
		List<T> result = new ArrayList<>();
		for(int i = 0; i < lists.size(); i++) {
			Task<QuerySnapshot> task = db.collection(collection).whereIn(position, lists.get(i)).get();
			double waiting = 0;
			while(!task.isComplete() && waiting <= TIMEOUT)
				waiting++;
			if(waiting > TIMEOUT)
				throw new TimeoutException(String.format("Out of time for getting %s", collection));
			for(DocumentSnapshot documentSnapshot : Objects.requireNonNull(task.getResult())) {
				T element = documentSnapshot.toObject(type);
				try {
					Method setId = type.getMethod("setId", String.class);
					if(setId != null) {
						setId.invoke(element, documentSnapshot.getId());
					}
				} catch(Exception e) {
				}
				result.add(element);
			}
		}
		return result;
	}
}
