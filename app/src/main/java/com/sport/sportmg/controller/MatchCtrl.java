package com.sport.sportmg.controller;

import android.util.Log;

import com.sport.sportmg.model.Equipe;
import com.sport.sportmg.model.Match;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class MatchCtrl {
	public Match[] getMatch(int limit, String filter) throws Exception {
		Dao dao = new Dao();
		Calendar today = Calendar.getInstance();
		int hour = 0, minutes = 0, seconds = 0;
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		if(filter.contains("latest")) {
			start = null;
		} else if(filter.contains("coming")) {
			end = null;
		} else if(filter.contains("today")) {
			start.set(Calendar.HOUR_OF_DAY, 0);
			start.set(Calendar.MINUTE, 0);
			start.set(Calendar.SECOND, 0);
			end.set(Calendar.HOUR_OF_DAY, 23);
			end.set(Calendar.MINUTE, 59);
			end.set(Calendar.SECOND, 59);
		} else { // Live

		}
		Date dateStart = null, dateEnd = null;
		if(start != null)
			dateStart = new Date(start.getTimeInMillis());
		if(end != null)
			dateEnd = new Date(end.getTimeInMillis());
		if(dateStart != null)
			Log.d("HEERE", "getMatch: " + dateStart.toString());
		List<Match> matchs = dao.select("match", "dateDebut", dateStart, dateEnd, limit, Match.class);
		Log.d("HEERE", "getMatch: " + matchs.size());
		Equipe[] equipes = new EquipeCtrl().getEquipes();
		setEquipes(matchs, equipes);
		if(matchs == null || matchs.size() == 0)
			return new Match[0];
		return matchs.toArray(new Match[0]);
	}

	private void setEquipes(List<Match> matchs, Equipe[] equipes) {
		for(Match match : matchs) {
			for(int i = 0; i < equipes.length; i++) {
				int n = 0;
				if(equipes[i].getId().equalsIgnoreCase(match.getDomicile().getEquipe().getId())) {
					match.getDomicile().setEquipe(equipes[i]);
					n++;
				}
				if(equipes[i].getId().equalsIgnoreCase(match.getExterieur().getEquipe().getId())) {
					match.getExterieur().setEquipe(equipes[i]);
					n++;
				}
				if(n == 2)
					break;
			}
		}
	}

	public Match[] searchMatch(String filter) throws Exception, TimeoutException {
		Dao dao = new Dao();
		List<Equipe> equipesList = dao.selectIgnoreCase("equipes", "nom", filter, Equipe.class);
		if(equipesList.size() == 0)
			return new Match[0];
		Equipe[] equipes = new Equipe[equipesList.size()];
		equipesList.toArray(equipes);
		List<String> ids = new ArrayList<>();
		for(Equipe e : equipes)
			ids.add(e.getId());
		List<Match> matchList = dao.select("match", "domicile.equipe.id", "exterieur.equipe.id", ids, Match.class);
		Match[] matches = new Match[matchList.size()];
		equipes = new EquipeCtrl().getEquipes();
		setEquipes(matchList, equipes);
		return matchList.toArray(matches);
	}
}