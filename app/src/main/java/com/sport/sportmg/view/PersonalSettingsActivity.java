package com.sport.sportmg.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.sport.sportmg.R;
import com.sport.sportmg.controller.UserCtrl;
import com.sport.sportmg.databinding.ActivityChangePasswordBinding;
import com.sport.sportmg.databinding.ActivityPersonalInfoBinding;
import com.sport.sportmg.model.User;
import com.sport.sportmg.utils.Utils;
import com.sport.sportmg.utils.UtilsView;

import java.security.NoSuchAlgorithmException;

public class PersonalSettingsActivity extends AppCompatActivity {
    ActivityChangePasswordBinding changePasswordBinding = null;
    ActivityPersonalInfoBinding personalInfoBinding = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String page = (String) getIntent().getSerializableExtra("page");
        if(page.equalsIgnoreCase("changePassword"))
            initChangePasswordPage();
        else
            initPersonalInfo();
    }

    private void initChangePasswordPage() {
        changePasswordBinding = ActivityChangePasswordBinding.inflate(getLayoutInflater());
        setContentView(changePasswordBinding.getRoot());
        UtilsView.toolbarBackBtn(changePasswordBinding.toolbar, this);
    }

    private void initPersonalInfo() {
        personalInfoBinding = ActivityPersonalInfoBinding.inflate(getLayoutInflater());
        setContentView(personalInfoBinding.getRoot());
        UtilsView.toolbarBackBtn(personalInfoBinding.toolbar, this);
    }

    public void onClickChangePassword(View view) {
        try {
            UserCtrl userCtrl = new UserCtrl();
            String pwd = Utils.sha1(changePasswordBinding.currentPasswordInput.getText().toString());
            String newPwd = changePasswordBinding.passwordInput.getText().toString();
            String confirmPwd = changePasswordBinding.passwordConfirmationInput.getText().toString();
            SharedPreferences shared = getSharedPreferences(UserCtrl.USER_PREFERENCE, Context.MODE_PRIVATE);
            String id = (shared.getString(UserCtrl.USER_ID, ""));
            User user = userCtrl.getUser(id);
            if(!user.getPwd().equals(pwd)) {
                Toast.makeText(this, R.string.wrong_pwd, Toast.LENGTH_SHORT).show();
                return;
            }
            if(newPwd.length() < 4) {
                Toast.makeText(this, R.string.too_short, Toast.LENGTH_SHORT).show();
                return;
            }
            if(!newPwd.equals(confirmPwd)) {
                Toast.makeText(this, R.string.not_match, Toast.LENGTH_SHORT).show();
                return;
            }
            newPwd = Utils.sha1(newPwd);
            boolean res = userCtrl.updatePwd(id, newPwd);
            if(res) {
                Toast.makeText(this, R.string.pwd_changed, Toast.LENGTH_SHORT).show();
                // TODO: 9/21/21 Go to setting page
            }
        } catch(Exception e) {
            Log.e("ERROR", e.getMessage(), e);
        }
    }

    public void onClickChangePersonalInfo(View view) {

    }
}
