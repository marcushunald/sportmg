package com.sport.sportmg.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.sport.sportmg.databinding.ActivitySettingsBinding;
import com.sport.sportmg.R;
import com.sport.sportmg.utils.UtilsView;

import java.util.Locale;

public class SettingsActivity extends AppCompatActivity {
    ActivitySettingsBinding binding = null;
    public static final String MODE = "mode";
    static Resources resources;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySettingsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        resources = getResources();
        init();
    }

    private void init() {
        UtilsView.toolbarBackBtn(binding.toolbar, this);
        getSupportFragmentManager().beginTransaction().replace(R.id.settings, new SettingsFragment()).commit();
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            addPreferencesFromResource(R.xml.settings);
        }

        @Override
        public boolean onPreferenceTreeClick(Preference preference) {
            Intent intent = null;
            switch (preference.getKey()) {
                case "personalInformation":
                case "changePassword":
                    intent = new Intent(getActivity(), PersonalSettingsActivity.class);
                    intent.putExtra("page", preference.getKey());
                    break;
                case "termsPolicy":
                    intent = new Intent(getActivity(), null);
                    break;
                case "about":
                    intent = new Intent(getActivity(), null);
                    break;
                case "mode":
                    if (preference.getSharedPreferences().getBoolean(preference.getKey(), false)) {
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    } else {
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    }
                    break;
//                case "language":
//                    preference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
//                        @Override
//                        public boolean onPreferenceChange(Preference preference, Object newValue) {
//                            String lang = newValue.toString();
//                            setAppLocale(preference.getSharedPreferences().getString(preference.getKey(), lang));
//                            return true;
//                        }
//                    });
//                    break;
            }
            if(intent != null)
                startActivity(intent);
            return true;
        }
    }

//    private static void setAppLocale(String localeCode){
//        DisplayMetrics dm = resources.getDisplayMetrics();
//        Configuration config = resources.getConfiguration();
//        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR1){
//            config.setLocale(new Locale(localeCode.toLowerCase()));
//        } else {
//            config.locale = new Locale(localeCode.toLowerCase());
//        }
//        resources.updateConfiguration(config, dm);
//    }
}
