package com.sport.sportmg.view.recyclerView;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sport.sportmg.R;
import com.sport.sportmg.model.Match;
import com.sport.sportmg.model.MatchDetails;
import com.sport.sportmg.view.MatchDetailsActivity;
import com.sport.sportmg.view.components.MatchCardView;
import com.sport.sportmg.view.components.TeamView;
import com.sport.sportmg.view.listener.OnCardMatchClickListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MatchCardAdapter extends RecyclerView.Adapter<MatchCardAdapter.ViewHolder> {

    private List<Match> matches;
    String cardSize = MatchCardView.CARD_SMALL;
    Context context;

    public MatchCardAdapter(Match[] match, String cardSize) {
        this.matches = Arrays.asList(match);
        this.cardSize = cardSize;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout = (cardSize.equalsIgnoreCase(MatchCardView.CARD_SMALL)) ? R.layout.view_match_card : R.layout.view_match_card_large;
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(layout, parent, false);
        return new ViewHolder(view, cardSize);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Match match = matches.get(position);
        holder.setCardView(match);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MatchDetailsActivity.class);
                intent.putExtra("match", match);
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return matches.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout leftTeam;
        private LinearLayout rightTeam;
        private TextView matchScore;
        private TextView matchDateTime;
        String viewHolderCardSize;
        View view;

        public ViewHolder(View view, String cardSize) {
            super(view);
            this.view = view;
            this.initComponents();
            this.viewHolderCardSize = cardSize;
        }


        private void setCardView(Match match) {
            final MatchDetails domicile = match.getDomicile();
            final MatchDetails exterieur = match.getExterieur();
            this.setLeftTeam(domicile.getEquipe().getNom(), domicile.getEquipe().getLogo());
            this.setRightTeam(exterieur.getEquipe().getNom(), exterieur.getEquipe().getLogo());
            this.setMatchScore(String.format("%d:%d", domicile.getScore(), exterieur.getScore()));
            this.setMatchDateTime(new SimpleDateFormat("HH:mm").format(match.getDateDebut()));
        }

        private void initComponents() {
            this.leftTeam = view.findViewById(R.id.leftTeam);
            this.rightTeam = view.findViewById(R.id.rightTeam);
            this.matchScore = view.findViewById(R.id.matchScore);
            this.matchDateTime = view.findViewById(R.id.matchDateTime);
        }

        public void setLeftTeam(CharSequence teamName, String teamPicUrl) {
            this.leftTeam.removeAllViewsInLayout();
            this.leftTeam.addView(new TeamView(view.getContext(), teamName, teamPicUrl, viewHolderCardSize));
        }

        public void setRightTeam(CharSequence teamName, String teamPicUrl) {
            this.rightTeam.removeAllViewsInLayout();
            this.rightTeam.addView(new TeamView(view.getContext(), teamName, teamPicUrl, viewHolderCardSize));
        }

        public void setMatchScore(CharSequence matchScore) {
            this.matchScore.setText(matchScore);
        }

        public void setMatchDateTime(CharSequence matchDateTime) {
            this.matchDateTime.setText(matchDateTime);
        }
    }
}
