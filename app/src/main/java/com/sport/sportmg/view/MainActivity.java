package com.sport.sportmg.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.NotificationCompat;
import androidx.preference.PreferenceManager;

import com.sport.sportmg.R;
import com.sport.sportmg.controller.UserCtrl;
import com.sport.sportmg.utils.UtilsNotification;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMode();
        SharedPreferences sharedPreferences = getSharedPreferences(UserCtrl.USER_PREFERENCE, Context.MODE_PRIVATE);
        Class cls = (sharedPreferences.contains(UserCtrl.USER_NAME)) ? TabActivity.class : LoginActivity.class;
        startActivity(new Intent(this, cls));
        finish();
    }

    private void setNotification() {

    }

    private void setMode() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        sharedPreferences.getBoolean(SettingsActivity.MODE, false);
        if (sharedPreferences.getBoolean(SettingsActivity.MODE, false)) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            return;
        }
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
    }
}
