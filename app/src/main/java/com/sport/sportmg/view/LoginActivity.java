package com.sport.sportmg.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.common.base.Strings;
import com.sport.sportmg.R;
import com.sport.sportmg.controller.UserCtrl;
import com.sport.sportmg.model.User;
import com.sport.sportmg.utils.UtilsView;

import java.util.Objects;
import java.util.concurrent.TimeoutException;

public class LoginActivity extends AppCompatActivity {
	private EditText username = null;
	private EditText password = null;
	private SharedPreferences sharedPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		initData();
	}

	private void initData() {
		username = findViewById(R.id.username);
		password = findViewById(R.id.password);
		sharedPreferences = getSharedPreferences(UserCtrl.USER_PREFERENCE, Context.MODE_PRIVATE);
	}


	public void onClickLoginBtn(View view) {
		try {
			if(!checkData())
				return;
//			ProgressDialog progressDialog = UtilsView.setProgressDialog(this, "dsq", getString(R.string.invalid_login), false);
//			progressDialog.show();
			UserCtrl userCtrl = new UserCtrl();
			User user = userCtrl.login(getValue(username), getValue(password));
			if(Objects.isNull(user)) {
				Toast.makeText(this, R.string.invalid_login, Toast.LENGTH_SHORT).show();
				return;
			}
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putString(UserCtrl.USER_NAME, user.getLogin());
			editor.putString(UserCtrl.USER_EMAIL, user.getEmail());
			editor.putString(UserCtrl.USER_ID, user.getId());
			editor.commit();
//			progressDialog.dismiss();
			pageRedirection();
		} catch(TimeoutException e) {
			Toast.makeText(this, R.string.connection_error, Toast.LENGTH_SHORT).show();
		} catch(Exception e) {
			Log.e("Error", e.getMessage());
		}
	}

	public void onClickSignupTxt(View view) {
		Intent intent = new Intent(this, SignupActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

	private String getValue(EditText editText) {
		return editText.getText().toString();
	}

	private boolean checkData() {
		if(Strings.isNullOrEmpty(getValue(username))) {
			Toast.makeText(this, R.string.login_required, Toast.LENGTH_SHORT).show();
			return false;
		}
		if(Strings.isNullOrEmpty(getValue(password))) {
			Toast.makeText(this, R.string.pwd_required, Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	private void pageRedirection() {
		Intent intent = new Intent(this, TabActivity.class);
		startActivity(intent);
		finish();
	}
}
