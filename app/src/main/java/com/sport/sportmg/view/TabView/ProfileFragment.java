package com.sport.sportmg.view.TabView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sport.sportmg.R;
import com.sport.sportmg.controller.UserCtrl;
import com.sport.sportmg.databinding.FragmentProfileBinding;
import com.sport.sportmg.view.LoginActivity;
import com.sport.sportmg.view.SettingsActivity;

public class ProfileFragment extends Fragment {

    FragmentProfileBinding binding = null;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentProfileBinding.inflate(inflater, container, false);
        SharedPreferences shared = getActivity().getSharedPreferences(UserCtrl.USER_PREFERENCE, Context.MODE_PRIVATE);
        binding.usernameProfile.setText(shared.getString(UserCtrl.USER_NAME, ""));
        init();
        return binding.getRoot();
    }

    private void init() {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.profilePreference, new ProfilePreferenceFragment()).commit();
    }

    public static class ProfilePreferenceFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            addPreferencesFromResource(R.xml.profile);
        }

        @Override
        public boolean onPreferenceTreeClick(Preference preference) {
            switch (preference.getKey()) {
                case "favoriteTeams":
                    Log.d("HEERE", "favorite");
                    break;
                case "remindedMatches":
                    Log.d("HEERE", "remindedMatches");
                    break;
                case "settings":
                    getActivity().startActivity(new Intent(getActivity(), SettingsActivity.class));
                    break;
                case "logout":
                    getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences(UserCtrl.USER_PREFERENCE, Context.MODE_PRIVATE);
                    sharedPreferences.edit().clear().commit();
                    break;
            }
            return true;
        }
    }
}