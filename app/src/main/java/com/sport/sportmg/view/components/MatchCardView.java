package com.sport.sportmg.view.components;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.sport.sportmg.databinding.ViewMatchCardLargeBinding;
import com.sport.sportmg.model.Match;
import com.sport.sportmg.model.MatchDetails;


import java.text.SimpleDateFormat;

public class MatchCardView extends LinearLayout {
    public static final String CARD_SMALL = "small";
    public static final String CARD_MEDIUM = "medium";
    public static final String CARD_LARGE = "large";

    private LinearLayout leftTeam;
    private LinearLayout rightTeam;
    private TextView matchScore;
    private TextView matchDateTime;

    private ViewMatchCardLargeBinding viewMatchCardLargeBinding;
    private String cardSize = CARD_SMALL;

    public MatchCardView(Context context) { super(context); }

    public MatchCardView(Context context, String cardSize, Match match) {
        super(context);
        //        inflate(context, R.layout.match_card_view, this);
        this.viewMatchCardLargeBinding = ViewMatchCardLargeBinding.inflate(LayoutInflater.from(context), this, true);
        this.init(match);
    }

    public MatchCardView(Context context, String cardSize) {
        super(context);
//        inflate(context, R.layout.match_card_view, this);
//        this.viewMatchCardLargeBinding = ViewMatchCardBinding.inflate(LayoutInflater.from(context), this, false);
//        this.cardSize = cardSize;
//        this.init();
    }

    public MatchCardView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    private void init(Match match) {
        this.initComponents();
        final MatchDetails domicile = match.getDomicile();
        final MatchDetails exterieur = match.getExterieur();
        setLeftTeam(domicile.getEquipe().getNom(), domicile.getEquipe().getLogo());
        setRightTeam(exterieur.getEquipe().getNom(), exterieur.getEquipe().getLogo());
        setMatchScore(String.format("%d:%d", domicile.getScore(), exterieur.getScore()));
        setMatchDateTime(new SimpleDateFormat("HH:mm").format(match.getDateDebut()));
//        this.editView();
    }

    private void initComponents() {
        this.leftTeam = viewMatchCardLargeBinding.leftTeam;
        this.rightTeam = viewMatchCardLargeBinding.rightTeam;
        this.matchScore = viewMatchCardLargeBinding.matchScore;
        this.matchDateTime = viewMatchCardLargeBinding.matchDateTime;
    }

    public void setLeftTeam(CharSequence teamName, String teamPicUrl) {
        this.leftTeam.addView(new TeamView(this.getContext(), teamName, teamPicUrl, this.cardSize));
    }

    public void setRightTeam(CharSequence teamName, String teamPicUrl) {
        this.rightTeam.addView(new TeamView(this.getContext(), teamName, teamPicUrl, this.cardSize));
    }

    public void setMatchScore(CharSequence matchScore) {
        this.matchScore.setText(matchScore);
    }

    public void setMatchDateTime(CharSequence matchDateTime) {
        this.matchDateTime.setText(matchDateTime);
    }

    public LinearLayout getLeftTeam() {
        return leftTeam;
    }

    public LinearLayout getRightTeam() {
        return rightTeam;
    }
//    private void editView() {
//        int cardWidth = LayoutParams.MATCH_PARENT;
//        int marginRight = 0;
//        if(this.cardSize.equalsIgnoreCase(this.CARD_SMALL)) {
//            cardWidth = LayoutParams.WRAP_CONTENT;
//            marginRight = R.dimen.space15;
//        }
//        this.viewMatchCardLargeBinding.matchCard.setLayoutParams(new LayoutParams(cardWidth, LayoutParams.WRAP_CONTENT));
//        LayoutParams params = (LinearLayout.LayoutParams) this.viewMatchCardLargeBinding.matchCard.getLayoutParams();
//        int margin = (int)getResources().getDimension(R.dimen.space10);
//        params.setMargins(0,margin,(marginRight == 0)? 0 : (int)getResources().getDimension(marginRight),margin);
//        this.viewMatchCardLargeBinding.matchCard.setLayoutParams(params);
//        this.viewMatchCardLargeBinding.matchCardContentContainer.setLayoutParams(new MaterialCardView.LayoutParams(cardWidth, LayoutParams.WRAP_CONTENT));
//    }
}
