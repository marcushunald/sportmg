package com.sport.sportmg.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.sport.sportmg.R;
import com.sport.sportmg.databinding.ActivityTabBinding;
import com.sport.sportmg.view.TabView.HomeFragment;
import com.sport.sportmg.view.TabView.NotificationFragment;
import com.sport.sportmg.view.TabView.ProfileFragment;
import com.sport.sportmg.view.TabView.SearchFragment;

public class TabActivity extends AppCompatActivity{
    ActivityTabBinding binding = null;

    HomeFragment homeFragment = new HomeFragment();
    SearchFragment searchFragment = new SearchFragment();
//    NotificationFragment notificationFragment = new NotificationFragment();
    ProfileFragment profileFragment = new ProfileFragment();
    FragmentManager fm = getSupportFragmentManager();
    Fragment activeFragment = homeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTabBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        init();
    }

    private void init() {
        loadFragment();
    }

    private void loadFragment() {
        fm.beginTransaction().add(R.id.navContainer, searchFragment).hide(searchFragment).commit();
//        fm.beginTransaction().add(R.id.navContainer, notificationFragment).hide(notificationFragment).commit();
        fm.beginTransaction().add(R.id.navContainer, profileFragment).hide(profileFragment).commit();
        fm.beginTransaction().add(R.id.navContainer, activeFragment).commit();
        binding.nav.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener(){
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.nav_home:
                        fm.beginTransaction().hide(activeFragment).show(homeFragment).commit();
                        activeFragment = homeFragment;
                        break;
                    case R.id.nav_search:
                        fm.beginTransaction().hide(activeFragment).show(searchFragment).commit();
                        activeFragment = searchFragment;
                        break;
//                    case R.id.nav_notif:
//                        fm.beginTransaction().hide(activeFragment).show(notificationFragment).commit();
//                        activeFragment = notificationFragment;
//                        break;
                    default:
                        fm.beginTransaction().hide(activeFragment).show(profileFragment).commit();
                        activeFragment = profileFragment;
                }
                return true;
            }
        });
    }

    private void initToolbar() {
        MaterialToolbar toolbar = binding.mainToolbar;
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.settings:
//                        TabActivity.this.startActivity(new Intent(TabActivity.this, ));
                        break;
                }
                return true;
            }
        });
    }
}