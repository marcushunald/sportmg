package com.sport.sportmg.view.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.sport.sportmg.R;
import com.sport.sportmg.view.recyclerView.MatchCardAdapter;
import com.squareup.picasso.Picasso;

public class TeamView  extends LinearLayout {
    private TextView teamName;
    private ImageView teamPic;
    private String cardSize = "small";

    public TeamView(Context context) { super(context); }

    public TeamView(Context context, CharSequence teamName, String teamPicUrl, String teamStyle) {
        super(context);
        this.init(context, teamName, teamPicUrl, teamStyle);
    }

    public TeamView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    private void init(Context context, CharSequence teamName, String teamPicUrl, String cardSize) {
//        int view = 0;
        this.cardSize = cardSize;
//        view = ;
//        switch(cardSize) {
//            case HomeMatchesAdapter.CARD_LARGE: view = R.layout.view_team_large;
//                break;
//            default: view = R.layout.view_team_small;
//                break;
//            default:
//                if(teamNamePosition != null) {
//                    switch(teamNamePosition) {
//                        case "right": view = R.layout.view_team_right_name;
//                            break;
//                        default: view = R.layout.view_team_left_name;
//                    }
//                }
//        }
        inflate(context, R.layout.view_team, this);
        this.initComponent();
        this.setTeamName(teamName);
        this.setTeamPic(teamPicUrl);
    }

    private void initComponent() {
        this.teamName = findViewById(R.id.teamName);
        this.teamPic = findViewById(R.id.teamPic);
        if(this.cardSize.equalsIgnoreCase(MatchCardView.CARD_SMALL)) {
            int maxWidth = (int)getResources().getDimension(R.dimen.card_max_width);
            this.teamName.setMaxWidth(maxWidth);
        }
    }

    public void setTeamName(CharSequence teamName) {
        this.teamName.setText(teamName);
    }

    public void setTeamPic(String teamPicUrl) {
        if(teamPicUrl != null && !teamPicUrl.equals(""))
            Picasso.get().load(teamPicUrl).into(this.teamPic);
        else
            this.teamPic.setImageResource(R.drawable.ic_sportmg_vector);
    }

    public void setView(String size) {
        if(size.equalsIgnoreCase("small")) {

        }
    }
}
