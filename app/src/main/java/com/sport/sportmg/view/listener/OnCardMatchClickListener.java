package com.sport.sportmg.view.listener;

import com.sport.sportmg.model.Match;

public interface OnCardMatchClickListener {
    void onCardMatchClick(Match match);
}
