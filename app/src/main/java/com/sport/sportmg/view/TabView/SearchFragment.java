package com.sport.sportmg.view.TabView;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.sport.sportmg.R;
import com.sport.sportmg.controller.MatchCtrl;
import com.sport.sportmg.databinding.FragmentSearchBinding;
import com.sport.sportmg.model.Match;
import com.sport.sportmg.view.MatchDetailsActivity;
import com.sport.sportmg.view.components.MatchCardView;
import com.sport.sportmg.view.listener.OnCardMatchClickListener;
import com.sport.sportmg.view.recyclerView.MatchCardAdapter;

public class SearchFragment extends Fragment {

    FragmentSearchBinding binding;

    public SearchFragment() {
        // Required empty public constructor
    }

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentSearchBinding.inflate(inflater, container, false);
        init();
        return binding.getRoot();
    }

    public void init() {
        // Listener to search bar
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        binding.searchMatchRecycleView.setLayoutManager(layoutManager);
        // set list
        MatchCtrl matchCtrl = new MatchCtrl();
        try {
            Match[] matchs = new Match[0];
            matchs = matchCtrl.getMatch(10, "latest");
            setMatchesList(matchs, MatchCardView.CARD_LARGE);
        } catch(Exception e) {
            Log.e("ERROR", e.getMessage(), e);
        }
        binding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String teamName) {
                if(teamName.length() >= 3) {
                    searchMatch(matchCtrl, teamName);
                } else if (teamName.length() == 0) {
                    try {
                        Match[] matches = matchCtrl.getMatch(10, "latest");
                        setMatchesList(matches, MatchCardView.CARD_LARGE);
                    } catch (Exception e) {
                        Log.e("ERROR", e.getMessage(), e);
                    }
                }
                return false;
            }
        });
    }

    public void searchMatch(MatchCtrl matchCtrl, String teamName) {
        Match[] matches = new Match[0];
        try {
            matches = matchCtrl.searchMatch(teamName);
            if(matches.length > 0) {
                binding.searchMatchRecycleView.setVisibility(View.VISIBLE);
                binding.teamNotFound.setVisibility(View.INVISIBLE);
                setMatchesList(matches, MatchCardView.CARD_LARGE);
            } else {
                binding.searchMatchRecycleView.setVisibility(View.INVISIBLE);
                binding.teamNotFound.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            Log.e("ERROR", "search error", e);
        }
    }

    private void setMatchesList(Match[] matches, String cardSize) {
        MatchCardAdapter matchCardAdapter = new MatchCardAdapter(matches, cardSize);
        binding.searchMatchRecycleView.setAdapter(matchCardAdapter);
    }
}