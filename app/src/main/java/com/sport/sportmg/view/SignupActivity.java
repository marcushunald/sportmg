package com.sport.sportmg.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.sport.sportmg.R;
import com.sport.sportmg.controller.UserCtrl;
import com.sport.sportmg.databinding.ActivitySignupBinding;
import com.sport.sportmg.model.User;
import com.sport.sportmg.utils.UtilsView;

import java.util.Objects;
import java.util.concurrent.TimeoutException;

public class SignupActivity extends AppCompatActivity {
    private SharedPreferences sharedPreferences;

    ActivitySignupBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignupBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        sharedPreferences = getSharedPreferences(UserCtrl.USER_PREFERENCE, Context.MODE_PRIVATE);
    }

    public void onClickRegisterBtn(View view) {
        if(!checkData())
            return;
        try {
            UserCtrl userCtrl = new UserCtrl();
            boolean userCreated = userCtrl.signUp(UtilsView.getEditTextValue(binding.username), UtilsView.getEditTextValue(binding.passwordInput), UtilsView.getEditTextValue(binding.emailInput));
            if(!userCreated) {
                Toast.makeText(this, R.string.invalid_signup, Toast.LENGTH_SHORT).show();
                return;
            }
            User user = userCtrl.login(UtilsView.getEditTextValue(binding.username), UtilsView.getEditTextValue(binding.passwordInput));
            if(Objects.isNull(user)) {
                Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show();
                return;
            }
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(UserCtrl.USER_NAME, user.getLogin());
            editor.putString(UserCtrl.USER_EMAIL, user.getEmail());
            editor.putString(UserCtrl.USER_ID, user.getId());
            editor.commit();
            pageRedirection();
        }  catch(TimeoutException e) {
            Toast.makeText(this, R.string.connection_error, Toast.LENGTH_SHORT).show();
        } catch(Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    public void onClickLoginTxt(View view) {
        finish();
    }

    private boolean checkData() {
        if(Strings.isNullOrEmpty(UtilsView.getEditTextValue(binding.username))) {
            Toast.makeText(this, R.string.login_required, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(Strings.isNullOrEmpty(UtilsView.getEditTextValue(binding.emailInput))) {
            Toast.makeText(this, R.string.email_required, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!UserCtrl.checkEmail(UtilsView.getEditTextValue(binding.emailInput))) {
            Toast.makeText(this, R.string.check_email, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(Strings.isNullOrEmpty(UtilsView.getEditTextValue(binding.passwordInput))) {
            Toast.makeText(this, R.string.pwd_required, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(Strings.isNullOrEmpty(UtilsView.getEditTextValue(binding.passwordConfirmationInput))) {
            Toast.makeText(this, R.string.pwd_conf_required, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!UserCtrl.checkPwdConf(UtilsView.getEditTextValue(binding.passwordInput), UtilsView.getEditTextValue(binding.passwordConfirmationInput))) {
            Toast.makeText(this, R.string.check_pwd, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void pageRedirection() {
        Intent intent = new Intent(this, TabActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}