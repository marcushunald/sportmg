package com.sport.sportmg.view.TabView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.sport.sportmg.R;
import com.sport.sportmg.controller.MatchCtrl;
import com.sport.sportmg.databinding.FragmentHomeBinding;
import com.sport.sportmg.model.Match;
import com.sport.sportmg.utils.UtilsView;
import com.sport.sportmg.view.MatchDetailsActivity;
import com.sport.sportmg.view.components.MatchCardView;
import com.sport.sportmg.view.listener.OnCardMatchClickListener;
import com.sport.sportmg.view.recyclerView.MatchCardAdapter;

import java.util.Arrays;

public class HomeFragment extends Fragment {
    FragmentHomeBinding binding = null;
    MatchCardAdapter liveAdapter;
    MatchCardAdapter matchesAdapter;
    String cardSize;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = FragmentHomeBinding.inflate(this.getLayoutInflater(), container, false);
        this.init();
        return binding.getRoot();
    }

    private void init() {
        try {
            setDaysMatch(R.string.latest);
            MatchCtrl matchCtrl = new MatchCtrl();
            if(getContext() == null) return; // avoid nullPointerException
            Match[] matchs = matchCtrl.getMatch(5, "latest".toLowerCase());
            this.setMatchesList(matchs, MatchCardView.CARD_SMALL, this.binding.liveMatchesRecyclerview);
            matchs = matchCtrl.getMatch(10, "latest".toLowerCase());
            this.setMatchesList(matchs, MatchCardView.CARD_LARGE, this.binding.matchesRecyclerview);
            this.setPopupFilterClick();
        } catch (Exception e) {
            Log.e("ERROR", e.getMessage(), e);
        }
    }

    private void setMatchesList(Match[] matches, String cardSize, RecyclerView recyclerView) {
        this.cardSize = cardSize;
        if (cardSize.equalsIgnoreCase(MatchCardView.CARD_SMALL)) {
            liveAdapter = new MatchCardAdapter(matches, cardSize);
            recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity(), LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setAdapter(liveAdapter);
        } else {
            matchesAdapter = new MatchCardAdapter(matches, cardSize);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setAdapter(matchesAdapter);
        }
    }


    public void setPopupFilterClick() {
        MaterialButton filterButton = this.binding.filterButton;
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(HomeFragment.this.getContext(), filterButton);
                popupMenu.getMenuInflater().inflate(R.menu.popup_home_filter, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        binding.daysMatchText.setText(menuItem.getTitle());
                        String matchDate = "";
                        switch (menuItem.getItemId()) {
                            case R.id.matchToday:
                                setDaysMatch(R.string.today);
                                matchDate="today";
                                break;
                            case R.id.matchLatest:
                                setDaysMatch(R.string.latest);
                                matchDate="latest";
                                break;
                            case R.id.matchUpComing:
                                setDaysMatch(R.string.up_coming);
                                matchDate="coming";
                                break;
                        }
                        try {
                            Match[] matchs = new MatchCtrl().getMatch(5, matchDate);
                            matchesAdapter = new MatchCardAdapter(matchs, HomeFragment.this.cardSize);
                            binding.matchesRecyclerview.setAdapter(matchesAdapter);
//                            Toast.makeText(HomeFragment.this.getContext(), "You Clicked " + menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            Log.e("ERROR", e.getMessage(), e);
                        }
                        return true;
                    }
                });
                // Showing the popup menu
                popupMenu.show();
            }
        });
    }

    private void setDaysMatch(int text) {
        binding.daysMatchText.setText(text);
        binding.daysMatchText.setAllCaps(true);
    }
}