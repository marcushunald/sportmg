package com.sport.sportmg.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.sport.sportmg.R;
import com.sport.sportmg.databinding.ActivityMatchDetailsBinding;
import com.sport.sportmg.model.Match;
import com.sport.sportmg.view.components.MatchCardView;
import com.sport.sportmg.view.recyclerView.MatchDetailsAdapter;

import java.net.URL;

public class MatchDetailsActivity extends AppCompatActivity {
    ActivityMatchDetailsBinding binding = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMatchDetailsBinding.inflate(this.getLayoutInflater());
        setContentView(binding.getRoot());
        this.init();
    }

    private void init() {
        Match match = (Match) getIntent().getSerializableExtra("match");
        this.initToolbarBnt();
        this.initMatchCard(match);
        RecyclerView recyclerView = binding.matchDetailRecycleView;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new MatchDetailsAdapter(this, match));
        setVideo(match);
    }

    private void initToolbarBnt() {
        Toolbar toolbar = binding.matchDetailsToolbar;
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                MenuItem menuItem = toolbar.getMenu().findItem(R.id.notifyMatch);
                menuItem.setIcon(R.drawable.outline_notifications_active_black_24dp);
                return true;
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MatchDetailsActivity.this.finish();
            }
        });
    }

    private void initMatchCard(Match match) {
        MatchCardView matchCardView = new MatchCardView(this, "large", match);
        matchCardView.getRightTeam().setOnClickListener(teamOnClickListener);
        matchCardView.getLeftTeam().setOnClickListener(teamOnClickListener);
        binding.matchCardContainer.addView(matchCardView);
    }

    private View.OnClickListener teamOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String teamName = (((TextView)findViewById(R.id.teamName)).getText()).toString();
            searchDefaultBrowser(teamName);
        }
    };

    private void searchDefaultBrowser(String teamName) {
        try {
            Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
            intent.putExtra(SearchManager.QUERY, teamName);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            searchBrowser(teamName);
        }
    }

    private void searchBrowser(String teamName) {
        try {
            Uri uri = Uri.parse("https://www.google.com/#q=" + teamName.replaceAll("\\s+","+"));
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Log.e("ERROR", e.getMessage(), e);
        }
    }

    private void setVideo(Match match) {
        if(match.getResumeUrl() == null && match.getResumeUrl().isEmpty()){
            binding.matchVideoContainer.setVisibility(View.GONE);
        } else {
            try {
                binding.matchVideoContainer.setVisibility(View.VISIBLE);
                VideoView videoView = binding.matchVideo;
                videoView.setVideoPath(match.getResumeUrl());
                MediaController mediaController = new MediaController(this);
                mediaController.setAnchorView(videoView);
                videoView.setMediaController(mediaController);
//                ProgressDialog progDailog = ProgressDialog.show(this, "Please wait ...", "Retrieving data ...", true);
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mediaController.show(0);
                    }
                });
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
            }
        }
    }

//    private MediaPlayer.OnPreparedListener mediaPreparedListener = new MediaPlayer.OnPreparedListener() {
//
//    };
}