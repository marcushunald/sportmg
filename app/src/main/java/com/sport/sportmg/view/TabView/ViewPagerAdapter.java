package com.sport.sportmg.view.TabView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ViewPagerAdapter extends FragmentStateAdapter {
    private static final int TAB_ITEM_SIZE = 4;

    public ViewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment currentFragment=null;
        switch (position) {
            case 0:
                currentFragment= HomeFragment.newInstance();
                break;
            case 1:
                currentFragment= SearchFragment.newInstance();
                break;
            case 2:
                currentFragment= NotificationFragment.newInstance();
                break;
            case 3:
                currentFragment= ProfileFragment.newInstance();
                break;
        }
        return currentFragment;
    }

    @Override
    public int getItemCount() {
        return TAB_ITEM_SIZE;
    }
}
