package com.sport.sportmg.view.recyclerView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sport.sportmg.databinding.RecycleViewMatchDetailsBinding;

import com.sport.sportmg.R;
import com.sport.sportmg.model.Match;
import com.sport.sportmg.model.MatchDesc;
import com.sport.sportmg.model.MatchDetails;

public class MatchDetailsAdapter extends RecyclerView.Adapter<MatchDetailsAdapter.ViewHolder> {
    private final Match match;
    RecycleViewMatchDetailsBinding binding = null;
    LayoutInflater layoutInflater = null;
    private MatchDesc[] matchDesc;

    public MatchDetailsAdapter(Context context, Match match) {
        layoutInflater = LayoutInflater.from(context);
        this.match = match;
        constructData();
    }

    private void constructData() {
        final MatchDetails domicile = match.getDomicile();
        final MatchDetails exterieur = match.getExterieur();
        matchDesc = new MatchDesc[6];
        matchDesc[0] = new MatchDesc(R.string.possession, domicile.getPossession(), exterieur.getPossession());
        matchDesc[1] = new MatchDesc(R.string.yellowCard, domicile.getCartonJaune(), exterieur.getCartonJaune());
        matchDesc[2] = new MatchDesc(R.string.redCard, domicile.getCartonRouge(), exterieur.getCartonRouge());
        matchDesc[3] = new MatchDesc(R.string.corner, domicile.getCorneur(), exterieur.getCorneur());
        matchDesc[4] = new MatchDesc(R.string.fouls, domicile.getFaute(), exterieur.getFaute());
        matchDesc[5] = new MatchDesc(R.string.shot, domicile.getTirsTotal(), exterieur.getTirsTotal());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = RecycleViewMatchDetailsBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getLeftTeamDetail().setText(String.valueOf(matchDesc[position].getDomicile()));
        holder.getRightTeamDetail().setText(String.valueOf(matchDesc[position].getExterieur()));
        holder.getMatchDetailName().setText(matchDesc[position].getName());
    }

    @Override
    public int getItemCount() {
        return matchDesc.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView leftTeamDetail;
        private final TextView rightTeamDetail;
        private final TextView matchDetailName;

        public ViewHolder(View view) {
            super(view);
            leftTeamDetail = view.findViewById(R.id.leftTeamDetail);
            rightTeamDetail = view.findViewById(R.id.rightTeamDetail);
            matchDetailName = view.findViewById(R.id.matchDetailName);
        }

        public TextView getLeftTeamDetail() {
            return leftTeamDetail;
        }

        public TextView getRightTeamDetail() {
            return rightTeamDetail;
        }

        public TextView getMatchDetailName() {
            return matchDetailName;
        }
    }

}
