package com.sport.sportmg.model;

public class User {
	private String id;
	private String login;
	private String pwd;
	private String email;

	public User() {
		super();
	}

	public User(String login, String email) {
		this.login = login;
		this.email = email;
	}

	public User(String login, String pwd, String email) {
		this.login = login;
		this.pwd = pwd;
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public String getPwd() {
		return pwd;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String toString() {
		return "User{" + "login='" + login + '\'' + ", email='" + email + '\'' + '}';
	}
}
