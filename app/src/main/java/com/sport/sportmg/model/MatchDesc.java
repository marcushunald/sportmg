package com.sport.sportmg.model;

public class MatchDesc {
	private int name;
	private int domicile;
	private int exterieur;

	public MatchDesc(int resId, int domicile, int exterieur) {
		this.name = resId;
		this.domicile = domicile;
		this.exterieur = exterieur;
	}

	public int getName() {
		return name;
	}

	public void setName(int name) {
		this.name = name;
	}

	public int getDomicile() {
		return domicile;
	}

	public void setDomicile(int domicile) {
		this.domicile = domicile;
	}

	public int getExterieur() {
		return exterieur;
	}

	public void setExterieur(int exterieur) {
		this.exterieur = exterieur;
	}
}
