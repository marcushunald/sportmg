package com.sport.sportmg.model;

import java.io.Serializable;

public class MatchDetails implements Serializable {
	private Equipe equipe = new Equipe();
	private int score = 1;
	private int possession = 60;
	private int cartonJaune = 3;
	private int cartonRouge = 1;
	private int corneur = 2;
	private int faute = 5;
	private int tirsTotal = 9;

	public Equipe getEquipe() {
		return equipe;
	}

	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}

	public int getScore() {
		return score;
	}

	public int getPossession() {
		return possession;
	}

	public int getCartonJaune() {
		return cartonJaune;
	}

	public int getCartonRouge() {
		return cartonRouge;
	}

	public int getCorneur() {
		return corneur;
	}

	public int getFaute() {
		return faute;
	}

	public int getTirsTotal() {
		return tirsTotal;
	}
}
