package com.sport.sportmg.model;

import java.io.Serializable;

public class Equipe implements Serializable {
	private String id = "";
	private String nom = "";
	private String logo = "";

	public Equipe() {
	}

	public Equipe(String nom, String logo) {
		this.nom = nom;
		this.logo = logo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getNom() {
		return nom;
	}

	public String getLogo() {
		return logo;
	}
}
