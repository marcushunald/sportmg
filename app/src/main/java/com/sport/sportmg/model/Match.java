package com.sport.sportmg.model;

import java.io.Serializable;
import java.util.Date;

public class Match implements Serializable {
	private Date dateDebut = new Date();
	private MatchDetails domicile = new MatchDetails();
	private MatchDetails exterieur = new MatchDetails();
	private String resumeUrl =  "";

	public Match() {
	}

	public Match(String dom, String ext) {
		domicile.getEquipe().setId(dom);
		exterieur.getEquipe().setId(ext);
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public MatchDetails getDomicile() {
		return domicile;
	}

	public MatchDetails getExterieur() {
		return exterieur;
	}

	public String getResumeUrl() {
		return resumeUrl;
	}

	@Override
	public String toString() {
		return "Match{" + "dateDebut=" + dateDebut + ", domicile=" + domicile + ", exterieur=" + exterieur + ", resumeUrl='" + resumeUrl + '\'' + '}';
	}
}
