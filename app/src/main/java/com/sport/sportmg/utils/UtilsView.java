package com.sport.sportmg.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class UtilsView {
    public static void toolbarBackBtn(Toolbar toolbar, Activity activity) {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { activity.finish(); }
        });
    }

    public static ProgressDialog setProgressDialog(Activity activity, String title, String message, boolean cancelable) {
        ProgressDialog progress = new ProgressDialog(activity);
        progress.setTitle(title);
        progress.setMessage(message);
        progress.setCancelable(cancelable); // disable dismiss by tapping outside of the dialog
        return progress;
    }

    public static String getEditTextValue(EditText editText) {
        return editText.getText().toString();
    }
}
