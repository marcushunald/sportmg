package com.sport.sportmg.utils;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.sport.sportmg.R;
import com.sport.sportmg.view.LoginActivity;
import com.sport.sportmg.view.MainActivity;

import java.util.HashMap;
import java.util.Map;

public class UtilsNotification {
    private static int notificationId = 0;
    // NOTIFICATION TYPE

    public static final String[] NOTIF_CHANNEL = {"general","match", "settings"};

    public UtilsNotification(){ }

    private static void createNotificationChannel(Activity activity, String channelId, String notifChannelName, String notifChannelDesc) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, notifChannelName, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(notifChannelDesc);
            NotificationManager notificationManager = activity.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public static NotificationCompat.Builder createNotification(Activity activity, String channelId) {
        return new NotificationCompat.Builder(activity, channelId);
    }

    public static void setNotification(Activity activity, NotificationCompat.Builder builder, int notificationId) {
        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(activity);
        managerCompat.notify(notificationId, builder.build());
    }

    public static void initNotificationChannel(Activity activity) {
        UtilsNotification.createNotificationChannel(activity, NOTIF_CHANNEL[0], "General", "General notifications");
        UtilsNotification.createNotificationChannel(activity, NOTIF_CHANNEL[1], "Matches", "Matches notifications");
        UtilsNotification.createNotificationChannel(activity, NOTIF_CHANNEL[2], "Settings", "Settings notifications");
    }

    public static void notifyOnAppExit(Activity activity) {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(activity, 0, intent, 0);

        UtilsNotification.initNotificationChannel(activity);
        NotificationCompat.Builder builder = UtilsNotification.createNotification(activity, UtilsNotification.NOTIF_CHANNEL[0]);
        builder.setDefaults(NotificationCompat.DEFAULT_SOUND);
        builder.setSmallIcon(R.drawable.ic_sportmg_vector)
                .setContentTitle("Come back soon")
                .setContentText("Hope you will come back soon too check new matches")
//            .setStyle(new NotificationCompat.BigTextStyle()
//                    .bigText("Much longer text that cannot fit one line..."))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent);
        UtilsNotification.setNotification(activity, builder, 0);
    }
}
